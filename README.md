# dogHUD

A modified version of Hypnotize's [Hypnotize HUD](https://github.com/Hypnootize/hypnotize-hud).

[Menu background](https://www.reddit.com/r/tf2/comments/cs7er8/tf2_scenebuild_view/) by [u/TheWildSpot](https://www.reddit.com/u/TheWildSpot/).

## Screenshots

<img src="screenshots/menu.jpg">
<img src="screenshots/mapselect.jpg">
<img src="screenshots/loadout.jpg">

## Notes

 - This is a messy project that is really intended for personal consumption only.

   - Most customizables have been removed (or are being removed) in order to make the project easier to maintain.
  
   - Changes may occur without warning.

 - The 'Favorite' button on the main menu just executes `favcmd`. Alias any command to `favcmd` to trigger it with that button:
```
alias favcmd "connect <your favorite server's IP>"
```

 - Full Linux compatibility is currently untested, but is a (future) goal.
